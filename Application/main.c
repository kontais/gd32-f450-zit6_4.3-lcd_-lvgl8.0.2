/*!
    \file  main.c
    \brief TLI_IPA demo 
*/

#include "gd32f4xx.h"
#include <stdio.h>
#include "gd32f450z_eval.h"
#include "gd32f450z_lcd.h"
#include "lv_conf.h"
#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_demo.h"

uint32_t timecount = 0;

void timer_tick_config(uint16_t pre, uint16_t psc);

/*!
    \brief      main program
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    lv_init();
    lv_port_disp_init();
	timer_tick_config(999, 199);
	lv_demo_benchmark();
	//lv_demo_widgets();

    while(1){
        lv_task_handler();
    }
}

/**
    \brief      configure the TIMER peripheral
    \param[in]  none
    \param[out] none
    \retval     none
  */
void timer_tick_config(uint16_t pre, uint16_t psc)
{
    /* ----------------------------------------------------------------------------
    TIMER9 Configuration: 
    time = (prescaler+1)*(period+1)*(repetitioncounter+1)/Tclk
    time/us and Tclk/Mhz
    (999+1)*(199+1)*(1)/200 = 1000us 
    ---------------------------------------------------------------------------- */
    timer_parameter_struct timer_initpara;
	
	nvic_irq_enable(TIMER0_UP_TIMER9_IRQn, 0, 1);

    /* enable the peripherals clock */
    rcu_periph_clock_enable(RCU_TIMER9);

    /* deinit a TIMER */
    timer_deinit(TIMER9);
    /* initialize TIMER init parameter struct */
    timer_struct_para_init(&timer_initpara);
    /* TIMER9 configuration */
    timer_initpara.prescaler         = pre;
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE;
    timer_initpara.counterdirection  = TIMER_COUNTER_UP;
    timer_initpara.period            = psc;
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1;
    timer_init(TIMER9, &timer_initpara);

    /* clear interrupt bit */
    timer_interrupt_flag_clear(TIMER9, TIMER_INT_FLAG_UP);
    /* enable the TIMER interrupt */
    timer_interrupt_enable(TIMER9, TIMER_INT_UP);
    /* enable a TIMER */
    timer_enable(TIMER9);
}
